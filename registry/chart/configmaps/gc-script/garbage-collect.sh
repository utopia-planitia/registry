#!/usr/bin/env ash
set -euxo pipefail

kubectl scale --replicas=2 --timeout=5m deployment/registry-ro
kubectl wait po -l readonly=true,name=registry --for condition=ready --timeout=5m
kubectl scale --replicas=0 --timeout=5m deployment/registry
kubectl delete po -l readonly=false,name=registry --grace-period=60 --ignore-not-found=true

POD=$(kubectl get po -l readonly=true,name=registry --output=jsonpath={.items..metadata.name} | cut -f 1 -d " ")
kubectl exec --tty=false --stdin=false "$POD" -- bin/registry garbage-collect /etc/docker/registry/config.yml

kubectl scale --replicas=2 --timeout=5m deployment/registry
kubectl wait po -l readonly=true,name=registry --for condition=ready --timeout=5m
kubectl scale --replicas=0 --timeout=5m deployment/registry-ro
