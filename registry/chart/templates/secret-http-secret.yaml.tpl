apiVersion: v1
kind: Secret
metadata:
  name: http-secret
  namespace: registry
type: generic
stringData:
  HTTP_SECRET: "{{ .Values.http_secret }}"
