#!/usr/bin/env bats

teardown () {
  kubectl delete -f internal-use.yml

  echo teardown log
  echo "exit code: $status"
  for i in "${!lines[@]}"; do 
    printf "line %s:\t%s\n" "$i" "${lines[$i]}"
  done
  echo teardown done
}

@test "push to internal registry, use image in pod" {
  export TAG=$(date +%s)
  export IMAGE="registry.registry.svc:5000/test-registry-alpine-internal:${TAG}"
  
  run skopeo copy --dest-tls-verify=false docker://alpine:3.6 docker://${IMAGE}
  [ $status -eq 0 ]

  run bash -ueo pipefail -c "cat internal-use.yml | envsubst | kubectl apply -f -"
  [ $status -eq 0 ]

  sleep 1

  run kubectl wait --for=condition=complete --timeout=120s job/internal-use-image
  [ $status -eq 0 ]
}
